function createRobots() {
    let eleMoreRobots = document.getElementById('moreRobots')
    const intervalTime = 400
    const reRoutingTime = 1200
    const numberOfDirection = 4
    document.getElementById('input').style.visibility = 'hidden'
    document.getElementById('color').style.visibility = 'hidden'
    document.getElementById('start').style.visibility = 'hidden'

    class ShowAndMove {
        constructor(number, color) {
            this.i = number
            console.log('Rob' + this.i )
            this.newRob = `<img src="${color}.png" alt="" id="robot${number}" width="100" height="100">`
            eleMoreRobots.innerHTML += this.newRob
            this.eleRob = document.getElementById(`robot${number}`)
            this.eleRob.style.left = this.getRandomPosition(window.innerWidth - this.eleRob.width) + 'px'
            this.eleRob.style.top = this.getRandomPosition(window.innerHeight - this.eleRob.height) + 'px'

            this.timer =  setInterval(this.getRandomDirection(numberOfDirection), intervalTime, this.i, this.eleRob)
            this.clearTimer = setInterval( () => {
                clearInterval(this.timer)
                this.timer = null
                this.timer =  setInterval(this.getRandomDirection(numberOfDirection), intervalTime, this.i, this.eleRob)
            }, reRoutingTime, this.i)
            console.log(this.i + ' position is ' + this.eleRob.offsetLeft)
        }

        getRandomPosition(max) {
            return Math.floor(Math.random() * Math.floor(max));
        }

        getRandomDirection(max) {
            this.direction = Math.floor(Math.random() * Math.floor(max));
            if (this.direction === 0) {
                return this.forward
            } else if (this.direction === 1) {
                return this.backward
            } else if (this.direction === 2) {
                return this.downward
            } else {
                return this.upward
            }
        }

        forward(i, ele) {
            console.log('Rob' + i + ' forward at: ' + ele.offsetLeft)
            if(ele.offsetLeft >= window.innerWidth - 2 * ele.width){
                ele.style.left = window.innerWidth - ele.width + 'px'
            } else {
                ele.style.left = ele.offsetLeft + 100 + 'px'
            }
        }

        backward(i, ele) {
            console.log('Rob' + i + ' backward at: '+ ele.offsetLeft)
            if (ele.offsetLeft <= ele.width) {
                ele.style.left = '0px'
            } else {
                ele.style.left = ele.offsetLeft - 100 + 'px'
            }
        }

        downward(i, ele) {
            console.log('Rob' + i + ' downward at: '+ ele.offsetTop)
            if (ele.offsetTop >= window.innerHeight - 2 * ele.height) {
                ele.style.top = window.innerHeight - ele.height + 'px'
            } else {
                ele.style.top = ele.offsetTop + 100 + 'px'
            }
        }

        upward(i, ele) {
            console.log('Rob' + i + ' upward at: '+ ele.offsetTop)
            if (ele.offsetTop <= ele.height) {
                ele.style.top = '0px'
            } else {
                ele.style.top = ele.offsetTop - 100 + 'px'
            }
        }

    }

    let numberInput = document.getElementById('input')
    let numberOfRobot = numberInput.value

    let colorSelect = document.getElementById('color')
    let colorOfRobot = colorSelect.value

    for (let i = 0; i < numberOfRobot; i++) {
        let robot = new ShowAndMove(i, colorOfRobot)
        console.log(robot.newRob)
    }
}